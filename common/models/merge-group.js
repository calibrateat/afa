'use strict'

module.exports = (Mergegroup) => {
  /* Mergegroup.afterRemote('find', function (ctx, user, next) {
    // console.log(ctx.method.name)
    if (ctx.result) {
      if (Array.isArray(ctx.result)) {
        ctx.result.forEach((_result) => {
          let _pageCount = 0
          let _jobCount = _result.mergeJobs.length
          console.log(_result.mergeJobs)
          for (let _job of _result.mergeJobs) {
            if (_job.fileInformation.hasOwnProperty('pages')) {
              _pageCount += _job.fileInformation.pages
            }
          }
          _result.jobsPageCount = _pageCount
          _result.jobsCount = _jobCount
        })
      } else {
        // console.log(ctx.result)
        let _result = ctx.result
        let _pageCount = 0
        let _jobCount = _result.mergeJobs.length
        for (let _job of _result.mergeJobs) {
          if (_job.fileInformation.hasOwnProperty('pages')) {
            _pageCount += _job.fileInformation.pages
          }
        }
        _result.jobsPageCount = _pageCount
        _result.jobsCount = _jobCount
      }
    }
    next()
  }) */
  // Remote method set status
  Mergegroup.toProduce = (cb) => {
    let _query = {
      'where': {
        'status': 'open'
      },
      'order': 'groupName ASC'
    }
    Mergegroup.find(_query, (err, _instances) => {
      if (err) {
        cb(err)
      } else {
        let _response = {'mergeGroup': []}
        _instances.forEach((_group) => {
          let _data = _group.toJSON()
          let _formatedGroup = {}
          _formatedGroup.name = _data.type
          _formatedGroup.numFiles = _data.mergeJobs.length // Fehler
          _formatedGroup.numPages = _data.aggregated.pageCount
          _formatedGroup.id = String(_group.getId())
          _response.mergeGroup.push(_formatedGroup)
        })
        cb(null, _response)
      }
    })
  }
  Mergegroup.remoteMethod(
    'toProduce', {
      'description': 'Return merge group how can be produced.',
      'http': [{
        'path': '/toProduce',
        'verb': 'get'
      }],
      'accepts': [],
      'returns': {
        'arg': 'data',
        'type': 'object',
        'root': true
      }
    }
  )
  // Remote method set status
  Mergegroup.produce = (groupId, productionGroupId, cb) => {
    Mergegroup.findById(groupId, (err, mergeInstance) => {
      if (err) {
        cb(err)
      }
      // change jobs status to 'toProduce'
      let _app = Mergegroup.app
      let _mergeJob = _app.models.MergeJob
      let _query = {
        'mergeGroupId': groupId
      }
      let _update = {
        'status': 'toProduce'
      }
      _mergeJob.updateAll(_query, _update, (err, jobInstances) => {
        if (err) {
          cb(err)
        } else {
          // update mergeInstance
          let _update = {
            'productionGroupId': productionGroupId,
            'status': 'toProduce'
          }
          mergeInstance.updateAttributes(_update, (err, updatedInstance) => {
            if (err) {
              cb(err)
            }
            cb(null, updatedInstance)
          })
        }
      })
    })
  }
  Mergegroup.remoteMethod(
    'produce', {
      'http': [{
        'path': '/:id/produce',
        'verb': 'post'
      }],
      'accepts': [{
        'arg': 'id',
        'type': 'string',
        'required': true,
        'http': {
          'source': 'path'
        }
      },
      {
        'arg': 'productionGroupId',
        'type': 'string',
        'required': true,
        'http': {
          'source': 'form'
        }
      }
      ],
      'returns': {
        'arg': 'data',
        'type': 'object',
        'root': true
      }
    }
  )
  // reporting on every loading
  Mergegroup.observe('loaded', (ctx, next) => {
    // console.log('loaded')
    if (ctx.data) {
      let _id = ctx.data.id
      // console.log(_id)
      let _app = Mergegroup.app
      let _mergeJob = _app.models.MergeJob
      let _query = {
        'where': {
          'mergeGroupId': _id
        },
        'fields': {
          'fileInformation': true
        }
      }
      _mergeJob.find(_query, (_err, _mergeJobs) => {
        var _pageCount = 0
        if (_err) {
          next(_err)
        } else if (_mergeJobs.length) {
          _mergeJobs.forEach((_job) => {
            _pageCount += _job.fileInformation.pages
          })
        }
        // ctx.data.queryAt = new Date()
        ctx.data.aggregated = {
          'pageCount': _pageCount
        }
        next()
      })
    } else {
      next()
    }
  })
  Mergegroup.xmlReport = (jobId, cb) => {
    cb(null, {
      'lala': [1, true]
    })
  }
  Mergegroup.remoteMethod('xmlReport', {
    'description': '',
    'accepts': [{
      'arg': 'id',
      'type': 'string',
      'required': true
    }],
    'http': {
      'path': '/:id/xmlreport',
      'verb': 'get'
    },
    'returns': {
      'type': 'object',
      'root': true
    }
  })
  // Remote method get status
  Mergegroup.getStatus = (jobId, cb) => {
    Mergegroup.findById(jobId, (err, instance) => {
      if (err) {
        cb(err)
      }
      var response = instance.status
      cb(null, response)
    })
  }
  Mergegroup.remoteMethod('getStatus', {
    'description': 'Retrurn the curent status.',
    'http': [{
      'path': '/:id/status',
      'verb': 'get'
    }],
    'accepts': {
      'arg': 'id',
      'type': 'any',
      'http': {
        'source': 'path'
      }
    },
    'returns': {
      'arg': 'status',
      'type': 'string'
    }
  })
  // Remote method set status
  Mergegroup.setStatus = (jobId, status, cb) => {
    Mergegroup.findById(jobId, (err, instance) => {
      if (err) {
        cb(err)
      }
      // instance.setId = jobId
      instance.updateAttribute('status', status, (err, updatedInstance) => {
        if (err) {
          cb(err)
        }
      })
      cb(null, instance)
    })
  }
  Mergegroup.remoteMethod(
    'setStatus', {
      'description': 'Change the status to given status',
      'http': [{
        'path': '/:id/status',
        'verb': 'post'
      }],
      'accepts': [{
        'arg': 'id',
        'type': 'any',
        'required': true,
        'http': {
          'source': 'path'
        }
      },
      {
        'arg': 'status',
        'type': 'any',
        'required': true,
        'http': {
          'source': 'form'
        }
      }
      ],
      'returns': {
        'arg': 'data',
        'type': 'object',
        'root': true
      }
    }
  )
}

# Ast Fischer API

## Installation

First  install mongo database on standard port 27017.

Get the this repro via git and test
```
git clone <url> <targetPath>
cd <targetPath>
npm i
node .
```

## Production (Live)
see https://blog.cloudboost.io/nodejs-pm2-startup-on-windows-db0906328d75

Install production tools
```
npm install --global --production windows-build-tools
```

Install pm2 globaly
```
npm install -g pm2
```

Windows set NODE_ENV to production

```
SET NODE_ENV=production
```

Unter Windows Server 2012 komme ich nicht weiter.
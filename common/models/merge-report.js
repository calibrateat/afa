'use strict'

module.exports = (Mergereport) => {
  // Observe befor save
  Mergereport.observe('before save', (ctx, next) => {
    var app = ctx.Model.app
    if (ctx.isNewInstance) {
      // local datasource name is localMongo
      let _mongoDb = app.dataSources.localMongo
      let _mongoConnector = _mongoDb.connector
      // ToDo change to findOneAndUpdate
      _mongoConnector.collection('Counter')
        .findAndModify({
          collection: 'MergeReport'
        }, [
          ['_id', 'asc']
        ], {
          $inc: {
            value: 1
          }
        }, {
          new: true
        }, (err, sequence) => {
          if (err) {
            throw err
          } else {
            ctx.instance.file_id = sequence.value.value
            next()
          }
        })
    } else {
      next()
    }
  })

  Mergereport.generate = (groupId, cb) => {
    let _app = Mergereport.app
    let _productionGroup = _app.models.ProductionGroup
    let _query = {
      'where': {
        'id': groupId
      },
      'include': 'mergeGroups'
    }
    _productionGroup.findOne(_query, (_err, _groupInstance) => {
      if (_err) {
        cb(_err)
      } else {
        if (_groupInstance) {
          let _data = _groupInstance.toJSON()
          let _response = {'items': {'item': []}}
          _data.mergeGroups.forEach((_group) => {
            _group.mergeJobs.forEach((_job) => {
              _response.items.item.push({
                'name': _job.filename,
                'status': 'produced',
                'created': _job.createdAt
              })
            })
          })
          Mergereport.create(_response, (err, _reportInstance) => {
            if (err) {
              cb(err)
            } else {
              _groupInstance.updateAttributes({
                'status': 'reported',
                'reportId': _reportInstance.getId()
              }, (err, _newMergeGroupInstance) => {
                if (err) {
                  cb(err)
                }
              })
              // delete _reportInstance.id ToDo
              let _data = JSON.parse(JSON.stringify(_reportInstance))
              cb(null, _data)
            }
          })
        } else {
          cb(null)
        }
      }
    })
  }

  Mergereport.remoteMethod(
    'generate', {
      'description': 'Generate a report by given type (error, produced).',
      'http': [{
        'path': '/generate',
        'verb': 'get'
      }],
      'accepts': [{
        'arg': 'groupId',
        'type': 'string',
        'required': true,
        'http': {
          'source': 'query'
        }
      }],
      'returns': [{
        'arg': 'data',
        'type': 'object',
        'root': true
      }]
    }
  )
  Mergereport.errors = (cb) => {
    let _app = Mergereport.app
    let _mergeGroup = _app.models.MergeGroup
    let _query = {
      'where': {
        'type': 'invalid',
        'status': 'open'
      }
    }
    _mergeGroup.findOne(_query, (_err, _mergeGroupInstance) => {
      // console.log(_mergeGroupInstance)
      if (_err) {
        cb(_err)
      } else if (_mergeGroupInstance) {
        let _data = _mergeGroupInstance.toJSON() // merci to Heinrich
        let _mergeJobs = _data.mergeJobs
        let _response = {'items': {'item': []}}
        _mergeJobs.forEach(_job => {
          _response.items.item.push({
            'name': _job.filename,
            'status': 'failed',
            'created': _job.createdAt
          })
        })
        Mergereport.create(_response, (err, _reportInstance) => {
          if (err) {
            cb(err)
          } else {
            _mergeGroupInstance.updateAttributes({
              'type': 'invalid',
              'status': 'reported',
              'reportId': _reportInstance.getId()
            }, (err, _newMergeGroupInstance) => {
              if (err) {
                cb(err)
              }
            })
            // delete _reportInstance.id ToDo
            let _data = JSON.parse(JSON.stringify(_reportInstance))
            cb(null, _data)
          }
        })
      } else {
        let _error = new Error('could no mergeGroup with errors find')
        _error.statusCode = 204
        /* let _error = {
          'error': {
            'message': 'could no mergeGroup with errors find',
            'stack': 'Error: could not find a invalid mergeGroup',
            'statusCode': 204
          }
        } */
        cb(null, '')
      }
    })
  }
  Mergereport.remoteMethod(
    'errors', {
      'description': 'Generate – if found – a report over error jobs and close set them to reported.',
      'http': [{
        'path': '/errors',
        'verb': 'get'
      }],
      'accepts': [],
      'returns': [{
        'arg': 'data',
        'type': 'object',
        'root': true
      }]
    }
  )
}

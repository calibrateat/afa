'use strict'

const __prefix__ = 'pg'

module.exports = (Productiongroup) => {
  Productiongroup.observe('before save', (ctx, next) => {
    if (ctx.isNewInstance) {
      let _now = new Date()
      let _name = __prefix__ + _now.getTime()
      ctx.instance.name = _name
      next()
    } else {
      next()
    }
  })
}

'use strict'

// const Ajv = require('ajv')
// let __ajv = new Ajv() // options can be passed, e.g. {allErrors: true}
// let __validate = __ajv.compile(require('./job.schema.json'))

// ToDo send more then one job

module.exports = (Mergejob) => {
  Mergejob.beforeRemote('create', (context, unused, next) => {
    let _data = context.args.data
    // console.log(JSON.stringify(context.args.data))
    // console.log(context.args.data.valid)
    // convert string in valid to boolean
    if (typeof _data.valid === 'string') {
      switch (_data.valid.toLowerCase()) {
        case 'yes':
        case '1':
        case 'true':
          _data.valid = true
          break
        default:
          _data.valid = false
      }
    }
    // Parse fileInformation
    if (typeof _data.fileInformation === 'string') {
      _data.fileInformation = JSON.parse(_data.fileInformation)
    }
    // can anybody know the mergeGroupId >> ToDo
    // get the current open group
    if (_data.groupName) {
      let _app = Mergejob.app
      let _mergeGroup = _app.models.MergeGroup
      let _whereQuery, _groupData
      if (_data.valid) {
        _whereQuery = {'where': {'type': _data.groupName, 'status': 'open'}}
        _groupData = {'type': _data.groupName, 'status': 'open'}
      } else {
        _whereQuery = {'where': {'type': 'invalid', 'status': 'open'}}
        _groupData = {'type': 'invalid', 'status': 'open'}
      }
      _mergeGroup.findOrCreate(_whereQuery, _groupData, (err, instance, created) => {
        // console.log(instance)
        // console.log(err)
        if (err) {
          console.log(err)
          next(err)
        } else {
          let _mergeGroupId = instance.getId()
          _data.mergeGroupId = _mergeGroupId
          next()
        }
      })
    } else {
      next('no groupName')
    }
  })
  /* Mergejob.afterRemote('create', (context, remoteMethodOutput, next) => {
    // console.log(context.args.data)
    let _data = context.args.data
    let _mergeGroupId = _data.mergeGroupId

    next()
  }) */
}
